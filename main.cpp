#include <iostream>
#include "MathematicalCalculations.h" // calculate
#include "EuropeanPrinter.h" // printDegrees

using namespace std;

int main(){
	float f;
	cout << "Enter the temperature in Farentheit degrees: ";
	cin >> f;
	printDegrees(calculate(f));
}
