/* function prints message about warmness feeling */

#include <iostream>
#include <iomanip> // setprecision, fixed
#include "EuropeanPrinter.h"

using namespace std;

void printDegrees(float c){
	cout << "The temperature is ";
	cout << fixed << setprecision(0) << c << "°C. ";
	if(c>=15){
	cout << "Outside is warm.\n";
	}else{
	cout << "Outside is cold.\n";
	}
}
